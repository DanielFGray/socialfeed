const WebSocket = require('ws')
const { Observable } = require('rxjs')
const request = require('superagent')
const {
  always,
  complement,
  compose,
  concat,
  converge,
  curry,
  equals,
  filter,
  flatten,
  identity,
  ifElse,
  join,
  last,
  lensProp,
  map,
  multiply,
  over,
  path,
  pipe,
  pluck,
  prop,
  propEq,
  propOr,
  sortBy,
  uniqBy,
} = require('ramda')
const Snoowrap = require('snoowrap')
const marked = require('marked')

const {
  github,
  gitlab,
  lastfm,
  reddit,
} = require('./secrets')

const $ = pipe
const c = compose
const propNotEq = complement(propEq)
const quote = str => `"${str}"`

const prepend = curry((a, b) => `${a}${b}`)

const pickOver = curry((spec, o) =>
  Object.entries(spec)
    .reduce((p, [k, fn]) => ({ ...p, [k]: fn(o) }), {}))

const seconds = date =>
  (date ? (new Date(date)).getTime() : Date.now())

const request$ = curry((base, header, method, endpoint, query = {}) =>
  Observable.defer(() => request[method](`${base}${endpoint}`).set(header).query(query))
    .map(x => x.body))

const gitlab$ = request$('https://gitlab.com/api/v4/', {
  'PRIVATE-TOKEN': gitlab.key,
})

const github$ = request$('https://api.github.com/', {
  Authorization: `token ${github.key}`,
})

const lastfm$ = (method, query) =>
  request$('http://ws.audioscrobbler.com/2.0/', {}, method, '', {
    api_key: lastfm.key,
    format: 'json',
    ...query,
  })

const stringify = x =>
  JSON.stringify(x, null, 2)

const Reddit = new Snoowrap({
  userAgent: 'my api thing',
  clientId: reddit.key,
  clientSecret: reddit.secret,
  username: reddit.user,
  password: reddit.password,
})

const feed$ = [
  github$('get', `users/${github.user}/gists`)
    .map($(
      map(pickOver({
        id: prop('id'),
        url: prop('html_url'),
        title: c(prepend('posted a Gist: '), ifElse(propOr(false, 'description'), prop('description'), $(prop('files'), Object.keys, join(', ')))),
        content: $(prop('files'), Object.keys, join(', ')),
        date: $(prop('updated_at'), seconds),
        type: always('gist'),
      })),
    )),
  github$('get', 'search/repositories', { q: `user:${github.user}` })
    .map($(
      prop('items'),
      map(pickOver({
        id: prop('name'),
        url: prop('html_url'),
        title: c(prepend('updated a repo on GitHub: '), prop('name')),
        content: prop('description'),
        date: $(prop('updated_at'), seconds),
        type: always('github'),
      })),
    )),
  gitlab$('get', `users/${gitlab.user}/projects`)
    .map($(
      filter(propNotEq('visibility', 'private')),
      map(pickOver({
        id: prop('name'),
        url: prop('web_url'),
        title: c(prepend('updated a repo on GitLab: '), prop('name')),
        content: prop('description'),
        date: $(prop('last_activity_at'), seconds),
        type: always('gitlab'),
      })),
    )),
  lastfm$('get', { method: 'user.getrecenttracks', limit: 50, user: lastfm.user })
    .map($(
      path(['recenttracks', 'track']),
      filter(prop('date')),
      map(pickOver({
        id: converge(concat, [prop('name'), path(['date', 'uts'])]),
        url: prop('url'),
        title: converge(concat, [
          c(prepend('listened to '), quote, prop('name')),
          c(prepend(' by '), path(['artist', '#text'])),
        ]),
        content: c(prepend('from the album '), quote, path(['album', '#text'])),
        date: $(path(['date', 'uts']), Number, multiply(1000)),
        type: always('lastfm'),
      })),
    )),
  Observable.defer(() => Reddit.getUser(reddit.user).getComments())
    .map($(
      map(pickOver({
        id: converge(concat, [prop('name'), prop('created_utc')]),
        url: prop('link_permalink'),
        title: $(prop('link_title'), prepend('commented on a Reddit post: ')),
        content: prop('body'),
        date: $(prop('created_utc'), Number, multiply(1000)),
        type: always('reddit'),
      })),
    )),
]

console.log('starting...')
const myFeed$ = Observable.timer(1, 10000)
  .do(console.log)
  .concatMap(() => Observable.combineLatest(feed$))
  .distinctUntilChanged((a, b) => {
    const lastDate = map($(pluck('date'), sortBy(identity), last))
    return equals(lastDate(a), lastDate(b))
  })
  .map($(
    flatten,
    uniqBy(prop('id')),
    sortBy(prop('date')),
    map(over(lensProp('content'), x => (x ? marked(x) : x))),
  ))
  .do(console.log)
  .publishReplay()
  .refCount()

const port = process.env.PORT || 8080
const wss = new WebSocket.Server({ port })

// Observable.create(o => { })

wss.on('connection', e => {
  console.log('listening')
  myFeed$
    .map(stringify)
    .subscribe(x => e.send(x), console.error)
})

wss.on('error', e => {
  console.log('error', e)
})

wss.on('close', () => {
  console.log('closed')
  myFeed$.unsubscribe()
})

process.on('uncaughtException', console.log)
